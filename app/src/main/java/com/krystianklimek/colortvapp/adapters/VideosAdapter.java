package com.krystianklimek.colortvapp.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.krystianklimek.colortvapp.R;
import com.krystianklimek.colortvapp.fragments.StartFragment;
import com.krystianklimek.colortvapp.models.Video;
import com.krystianklimek.colortvapp.models.Videos;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.krystianklimek.colortvapp.MainActivity.sFragmentManager;
import static com.krystianklimek.colortvapp.fragments.StartFragment.sListener;
import static com.krystianklimek.colortvapp.utils.Utils.minutesToHours;

/**
 * Created by: Krystian Klimek
 * Date: 21.10.2016.
 */

public class VideosAdapter extends RecyclerView.Adapter<VideosAdapter.ViewHolder> {
    private Context mContext;
    private List<Video> mVideosList;

    public VideosAdapter(Context context, Videos videos) {
        this.mContext = context;
        this.mVideosList = videos.getVideos();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.recycler_view_single_item, parent, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final Video video = mVideosList.get(position);

        holder.title.setText(video.getTitle());
        holder.desc.setText(video.getDescription());
        holder.duration.setText(minutesToHours(video.getDurationInMinutes()));
        holder.rating.setText(String.valueOf(video.getRating()));
        holder.viewsCount.setText(video.getViewsCount());

        Glide
            .with(mContext)
            .load(video.getThumbnailUrl())
            .fallback(R.color.recycler_view_background)
            .error(R.color.recycler_view_background)
            .centerCrop()
            .crossFade()
            .diskCacheStrategy(DiskCacheStrategy.SOURCE)
            .into(holder.photo);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sFragmentManager.popBackStack();
//                sListener.onDataChanged(video.getVideoId());
                StartFragment.sVideoId = video.getVideoId();
            }
        });
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return mVideosList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.titleTextView)
        TextView title;
        @BindView(R.id.descTextView)
        TextView desc;
        @BindView(R.id.durationTextView)
        TextView duration;
        @BindView(R.id.ratingTextView)
        TextView rating;
        @BindView(R.id.viewsCountTextView)
        TextView viewsCount;
        @BindView(R.id.photoImageView)
        ImageView photo;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
