package com.krystianklimek.colortvapp.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.krystianklimek.colortvapp.R;
import com.krystianklimek.colortvapp.utils.Utils;
import com.krystianklimek.colortvapp.adapters.VideosAdapter;
import com.krystianklimek.colortvapp.models.Videos;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.krystianklimek.colortvapp.MainActivity.sFragmentManager;
import static com.krystianklimek.colortvapp.utils.Utils.showToast;
import static com.krystianklimek.colortvapp.fragments.StartFragment.ID_KEY;

/**
 * Created by: Krystian Klimek
 * Date: 21.10.2016.
 */

public class VideosFragment extends Fragment{
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    private String mId = null;
    private Videos mVideos;

    public static VideosFragment newInstance(Bundle args) {
        VideosFragment fragment = new VideosFragment();
        fragment.setArguments(args);

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        return super.onCreateView(inflater, container, savedInstanceState);

        View fragment = inflater.inflate(R.layout.fragment_videos, container, false);
        ButterKnife.bind(this, fragment);

        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setHomeButtonEnabled(true);

        getArgs();

        if (mId == null) {
            showToast(getActivity(), getString(R.string.no_valid_data));
        } else {
            getJsonData();
            initRecyclerView();
        }

        return fragment;
    }

    private void getArgs() {
        Bundle arguments = getArguments();
        if (arguments != null && arguments.containsKey(ID_KEY))
            mId = getArguments().getString(ID_KEY);
    }

    private void getJsonData() {
        String dataset = Utils.loadJSONFromAsset(getActivity(), "dataset" + mId);

        JsonParser parser = new JsonParser();
        JsonElement mJson =  parser.parse(dataset);

        Gson gson = new Gson();

        mVideos = gson.fromJson(mJson, Videos.class);
    }

    private void initRecyclerView() {
        VideosAdapter adapter = new VideosAdapter(getActivity(), mVideos);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(adapter);
    }
}