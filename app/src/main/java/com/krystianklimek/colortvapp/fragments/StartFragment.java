package com.krystianklimek.colortvapp.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.krystianklimek.colortvapp.R;
import com.krystianklimek.colortvapp.utils.OnChangeTextListener;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.krystianklimek.colortvapp.MainActivity.sFragmentManager;

/**
 * Created by: Krystian Klimek
 * Date: 21.10.2016.
 */

public class StartFragment extends Fragment {

    @BindView(R.id.videoIdTextView)
    TextView videoIdTextView;

    public static final String ID_KEY = "ID_KEY";
    public static String sVideoId = null;

    public static OnChangeTextListener sListener;

    public StartFragment() {

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        return super.onCreateView(inflater, container, savedInstanceState);

        View fragment = inflater.inflate(R.layout.fragment_start, container, false);
        ButterKnife.bind(this, fragment);

        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(false);

        setVideoId();

        return fragment;
    }

    private void setVideoId() {
        if (sVideoId == null)
            videoIdTextView.setVisibility(View.GONE);
        else {
            String text = getActivity().getResources().getString(R.string.clicked_video_id) + sVideoId;
            videoIdTextView.setText(text);
            videoIdTextView.setVisibility(View.VISIBLE);
        }
    }

    @OnClick({R.id.firstButton, R.id.secondButton, R.id.thirdButton})
    public void onButtonClick(Button button) {
        Bundle args = new Bundle();
        args.putString(ID_KEY, button.getTag().toString());

        VideosFragment fragment = VideosFragment.newInstance(args);

        sFragmentManager
                .beginTransaction()
                .replace(R.id.container, fragment)
                .addToBackStack(fragment.getClass().getSimpleName())
                .commit();
    }
}