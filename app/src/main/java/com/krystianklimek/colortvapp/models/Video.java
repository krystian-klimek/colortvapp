package com.krystianklimek.colortvapp.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by: Krystian Klimek
 * Date: 21.10.2016.
 */

public class Video {

    @SerializedName("videoId")
    @Expose
    private String videoId;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("durationInMinutes")
    @Expose
    private int durationInMinutes;
    @SerializedName("rating")
    @Expose
    private double rating;
    @SerializedName("viewsCount")
    @Expose
    private String viewsCount;
    @SerializedName("thumbnailUrl")
    @Expose
    private String thumbnailUrl;

    /**
     *
     * @return
     *     The videoId
     */
    public String getVideoId() {
        return videoId;
    }

    /**
     *
     * @param videoId
     *     The videoId
     */
    public void setVideoId(String videoId) {
        this.videoId = videoId;
    }

    /**
     *
     * @return
     *     The title
     */
    public String getTitle() {
        return title;
    }

    /**
     *
     * @param title
     *     The title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     *
     * @return
     *     The description
     */
    public String getDescription() {
        return description;
    }

    /**
     *
     * @param description
     *     The description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     *
     * @return
     *     The durationInMinutes
     */
    public int getDurationInMinutes() {
        return durationInMinutes;
    }

    /**
     *
     * @param durationInMinutes
     *     The durationInMinutes
     */
    public void setDurationInMinutes(int durationInMinutes) {
        this.durationInMinutes = durationInMinutes;
    }

    /**
     *
     * @return
     *     The rating
     */
    public double getRating() {
        return rating;
    }

    /**
     *
     * @param rating
     *     The rating
     */
    public void setRating(double rating) {
        this.rating = rating;
    }

    /**
     *
     * @return
     *     The viewsCount
     */
    public String getViewsCount() {
        return viewsCount;
    }

    /**
     *
     * @param viewsCount
     *     The viewsCount
     */
    public void setViewsCount(String viewsCount) {
        this.viewsCount = viewsCount;
    }

    /**
     *
     * @return
     *     The thumbnailUrl
     */
    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    /**
     *
     * @param thumbnailUrl
     *     The thumbnailUrl
     */
    public void setThumbnailUrl(String thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;
    }

}

