package com.krystianklimek.colortvapp.models;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by: Krystian Klimek
 * Date: 21.10.2016.
 */

public class Videos {

    @SerializedName("videos")
    @Expose
    private List<Video> videos = new ArrayList<Video>();

    /**
     *
     * @return
     *     The videos
     */
    public List<Video> getVideos() {
        return videos;
    }

    /**
     *
     * @param videos
     *     The videos
     */
    public void setVideos(List<Video> videos) {
        this.videos = videos;
    }

}