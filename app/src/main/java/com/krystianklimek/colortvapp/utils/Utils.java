package com.krystianklimek.colortvapp.utils;

import android.content.Context;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by: Krystian Klimek
 * Date: 20.10.2016.
 */

public class Utils {
    public static String loadJSONFromAsset(Context context, String dataset) {
        String json = null;
        try {

            InputStream is = context.getAssets().open(dataset);

            int size = is.available();

            byte[] buffer = new byte[size];

            is.read(buffer);

            is.close();

            json = new String(buffer, "UTF-8");


        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    public static void showToast(Context context, String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }

    public static String minutesToHours(int minutes) {
        int h = minutes/60;
        int min = minutes%60;

        return h==0 ? min + "m" : h + "h " + min + "m";
    }
}
