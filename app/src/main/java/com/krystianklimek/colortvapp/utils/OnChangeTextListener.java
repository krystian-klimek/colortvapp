package com.krystianklimek.colortvapp.utils;

/**
 * Created by: Krystian Klimek
 * Date: 21.10.2016.
 */

public interface OnChangeTextListener {
    void onDataChanged(String text);
}
